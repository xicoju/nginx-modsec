FROM ubuntu:14.04

RUN apt-get update && apt-get upgrade -y && apt-get install -y bison build-essential curl dh-autoreconf apache2-dev \
  doxygen flex g++ geoip-bin git libcurl4-gnutls-dev libgd-dev libgd3 libgeoip-dev libgeoip1 libpcre++-dev \
  libpcre3 libpcre3-dev libperl-dev libtool libxml2 libxml2-dev libxslt1-dev libxslt1.1 libyajl-dev perl zlib1g-dev \
  libgoogle-perftools-dev \
  && cd /opt \
  && curl -L -O https://www.openssl.org/source/old/0.9.x/openssl-0.9.8zh.tar.gz \
  && curl -L -O https://github.com/SpiderLabs/ModSecurity/releases/download/v2.9.3/modsecurity-2.9.3.tar.gz \
  && curl -L -O http://nginx.org/download/nginx-1.6.2.tar.gz \
  && curl -L -O https://raw.githubusercontent.com/SpiderLabs/ModSecurity/v3/master/modsecurity.conf-recommended \
  && tar xf openssl-0.9.8zh.tar.gz \
  && tar xf modsecurity-2.9.3.tar.gz \
  && tar xf nginx-1.6.2.tar.gz \
  && rm -rf /opt/*.tar.gz \
  && (cd openssl-0.9.8zh && ./config shared --prefix=/usr/local/openssl/ && make && make install) \
  && (cd modsecurity-2.9.3 && ./autogen.sh && ./configure --enable-standalone-module --disable-mlogc && make) \
  && cd nginx-1.6.2 \
  && ./configure --prefix=/usr/share/nginx --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log --http-client-body-temp-path=/var/lib/nginx/tmp/client_body \
    --http-fastcgi-temp-path=/var/lib/nginx/tmp/fastcgi --http-log-path=/var/log/nginx/access.log \
    --http-proxy-temp-path=/var/lib/nginx/tmp/proxy --http-scgi-temp-path=/var/lib/nginx/tmp/scgi \
    --http-uwsgi-temp-path=/var/lib/nginx/tmp/uwsgi --lock-path=/var/lock/subsys/nginx --pid-path=/var/run/nginx.pid \
    --with-file-aio --with-google_perftools_module --with-http_addition_module --with-http_auth_request_module \
    --with-http_dav_module --with-http_degradation_module --with-http_flv_module --with-http_geoip_module \
    --with-http_gunzip_module --with-http_gzip_static_module --with-http_image_filter_module --with-http_mp4_module \
    --with-http_perl_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module \
    --with-http_spdy_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module \
    --with-http_xslt_module --with-ipv6 --with-mail --with-mail_ssl_module --with-openssl=/opt/openssl-0.9.8zh \
    --with-pcre --with-pcre-jit --add-module=/opt/modsecurity-2.9.3/nginx/modsecurity \
  && make \
  && make install \
  && cd /opt \
  && mv /opt/modsecurity.conf-recommended /etc/nginx/modsecurity.conf \
  && mkdir -p /var/lib/nginx/tmp/ \
  && apt-get remove -y build-essential g++ git doxygen \
  && apt-get autoremove -y \
  && apt-get autoclean -y

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
  && ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80 8080

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
